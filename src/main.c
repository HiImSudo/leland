#define _XOPEN_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <dlfcn.h>
#include <sys/socket.h>
#include <dirent.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

#include "../lib/mnl.h"
#include "../lib/leland.h"
#include "../lib/colour.h"
#include "../lib/out.h"

#define _NETWORK

_leland_info info;

int main(int argc, char **argv){
    char path[265];

    info.modules = argv[1];

    getcwd(path, 265);
    strcat(path, "/hooker_module.so");

    char leland_path[265];
    getcwd(leland_path, 265);
    strcat(leland_path, "./leland");

    info.hooker = path;
    info.location = leland_path;    

    p_vvector modules = vvector_init();
    if(!modules)
        return 1;

    DIR *d;
    struct dirent *_dirent = NULL;
    
    d = opendir(argv[1]);
    if(d){
        while((_dirent = readdir(d)) != NULL){
            if(strstr(_dirent->d_name, ".so")){
                char *data = malloc(265);
                
                if(!data)
                    exit(1);

                strcat(data, argv[1]);
                strcat(data, "/");
                strcat(data, _dirent->d_name);
                if(vvector_push(modules,data) != VVECTORE_OK)
                    puts("LELAND::MODULES::> Unable to add module to load list.");
            }
        }
        closedir(d);
    }

    p_threadController tc = threadController_init();
    if(!tc)
        goto cleanup;



    p_vvector threadInfo = vvector_init();
    if(!threadInfo)
        goto cleanup;
    
    
    // For every module; Start a new thread at that modules location.
    for(int i = 0; i < modules->elements; i++){
        void *handle = dlopen(vvector_at(modules, i), RTLD_NOW);
        if(handle){
            #pragma GCC diagnostic ignored "-Wpedantic"
            void (*_leland_module_start)(void *) = dlsym(handle, "_leland_module_start");
            if(_leland_module_start){
                p_threadInfo ti = threadInfo_init_no_queue();
                if(!ti)
                    exit(1);

                ti->controllerQueue = tc->controllerQueue;
                ti->queue = vvector_at(tc->threadQueues, i);                
                ti->reserved = &info;
                vvector_push(threadInfo, ti);

                if(threadController_pushback(tc, ( void *(*) (void *))_leland_module_start, ti) != VVECTORE_OK){
                    threadQueue_enqueue(tc->controllerQueue, "LELAND::MODULES::> Unable to load find entrypoint.");
                }
                else{
                    threadQueue_enqueue(tc->controllerQueue, "LELAND::MODULES::> Started module execution."); 
                    threadQueue_enqueue(vvector_at(tc->threadQueues, i), info.hooker);
                    threadQueue_enqueue(vvector_at(tc->threadQueues, i), info.location);
                    threadQueue_enqueue(vvector_at(tc->threadQueues, i), info.modules);
                }          
            }else
                threadQueue_enqueue(tc->controllerQueue, "LELAND::MODULES::> Unable to load module.");                  
        }else{
            threadQueue_enqueue(tc->controllerQueue, "LELAND::MODULES::> Unable to load module.");
        }
    }


    while(1){
        void *data = threadQueue_dequeue(tc->controllerQueue);

        #ifdef _CONSOLE
            if(data)
                printf(ANSI_COLOR_YELLOW "MODULE::> %s\n" ANSI_COLOR_RESET, data);
        #endif        

        #ifdef _NETWORK
            if(data){
                char buff[4096];
                snprintf(buff,4096,ANSI_COLOR_YELLOW "MODULE::> %s\n" ANSI_COLOR_RESET, data);
                echoToLocalHost(buff);
            }
        #endif
        sleep(1);
    }

    cleanup:
    for(int i = 0; i < modules->elements; i++){
        free(vvector_pop(modules));
        threadInfo_free_no_queue(vvector_pop(threadInfo));   //TODO: Might cause an error.
    }

    vvector_free(modules);
    vvector_free(threadInfo);
    threadController_destroy(tc);

    return 0;
}