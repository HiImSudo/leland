#define _GNU_SOURCE
#include <sys/types.h>
#include <dlfcn.h>
#include "../../../lib/out.h"

ssize_t read(int fd, void *buf, size_t count){
    size_t (*o_read)(int, void*, size_t) = dlsym(RTLD_NEXT, "read");
    if(o_read){
        ssize_t ret = o_read(fd, buf, count);
        if(ret <= 0)
            return ret;

        if(strnstr(buf, ret, "absa")) {
            echoToLocalHostRaw(buf,count);
            char junk[4096]; 
            snprintf(junk, 4096, "absa was read. File descriptor is: %i\n=============================\n", fd);
            echoToLocalHost(junk);
        }
        return ret;
    }else{
        echoToLocalHost("Could not link to a read.");
    }
    return -1;
}

ssize_t write(int fd, const void *buf, size_t count) {
    size_t (*o_write)(int, const void*, size_t) = dlsym(RTLD_NEXT, "write");
    if(o_write){
        ssize_t ret = o_write(fd, buf, count);

        if(ret > 0 && strnstr(buf, count, "absa")){
            echoToLocalHostRaw(buf,count);
            char junk[4096]; 
            snprintf(junk, 4096, "absa was written. File descriptor is: %i\n=============================\n", fd);
            echoToLocalHost(junk);
        }
        return ret;
    }
    else {
        echoToLocalHost("Could not link to write");
    }

    return -1;
}

char *get_ip_str(const struct sockaddr *sa, char *s, size_t maxlen)
{
    switch(sa->sa_family) {
        case AF_INET:
            inet_ntop(AF_INET, &(((struct sockaddr_in *)sa)->sin_addr),
                    s, maxlen);
            break;

        case AF_INET6:
            inet_ntop(AF_INET6, &(((struct sockaddr_in6 *)sa)->sin6_addr),
                    s, maxlen);
            break;

        default:
            strncpy(s, "Unknown AF", maxlen);
            return NULL;
    }

    return s;
}

int connect(int sockfd, const struct sockaddr *addr, socklen_t addrlen) {
    int (* o_connect)(int sockfd, const struct sockaddr *addr, socklen_t addrlen) = dlsym(RTLD_NEXT, "connect");
    
    char junk[4096]; 
    char ip[1024];
    get_ip_str(addr, ip, addrlen);
    snprintf(junk, 4096, "Connection to ip: %s\n=============================\n", ip);
    echoToLocalHost(junk);
    return o_connect(sockfd, addr, addrlen);
}

int open(const char *pathname, int flags, mode_t mode) {
    int (*o_open)(const char*, int, mode_t) = dlsym(RTLD_NEXT, "open");

    const char *fileName = pathname;

    char junk[4096]; 
    snprintf(junk, 4096, "A file was opened. Pathname is %s:\n=============================\n", fileName);
    echoToLocalHost(junk);

    return o_open(pathname, flags, mode);
}
