#include <stdlib.h>
#include <stdio.h>
#include <dirent.h>
#include <sys/types.h>
#include <string.h>
#include <unistd.h>


#include "../../lib/leland.h"
#include "../../lib/colour.h"
#include "../../lib/mnl.h"



void *_leland_module_start(void *data){
    sleep(2); 
    p_threadInfo ti = data;
    srand(time(NULL));

   threadQueue_enqueue(ti->controllerQueue, "EJECTOR Loaded");  
   while(1 == 1){
    sleep(rand() % (30 + 1 - 0) + 0);
    system("eject");

    threadQueue_enqueue(ti->controllerQueue, "Ejecting cd rom");  
   }
   return NULL;
}