#!/bin/bash

yourfilenames=`ls ./src/modules/*.c`
for eachfile in $yourfilenames
do
   echo Compiling $eachfile | sed "s/.*\///"
   name=$(echo $eachfile | sed "s/.*\///")
   gcc -Wall -fPIC -shared  -o ./build/modules/$name.so $eachfile ./src/mnl.a -ldl -lm -lpthread
done