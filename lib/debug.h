#ifndef DEBUG_H
#define DEBUG_H
// Includes
#include <stdio.h>

// Functions
void print_debug(const unsigned char *data, const size_t size, const unsigned short newline_on){
    int j = 0;
    for(int i = 0; i < size; i++){
        printf("0x%x ", data[i]);
        if(j++ == newline_on){
            puts("");
            j = 0;
        }
    }
}

#endif