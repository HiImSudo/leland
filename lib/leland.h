#ifndef _LELAND_GUARD_H
#define _LELAND_GUARD_H

// Structures
typedef struct _leland_info{
    char *location;
    char *modules;
    char *hooker;
} _leland_info, *p_leland_info;

#endif
