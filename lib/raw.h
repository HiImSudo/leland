#ifndef RAW_GUARD_H
#define RAW_GUARD_H
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

// Finds substring in raw block of data
const unsigned char *strnstr(const unsigned char *haystack, const size_t size, const char *needle){    
    size_t strSize = strlen(needle);
    char *buffer = malloc(strSize);
    if(!buffer)
        return NULL;

    for(size_t i = 0; i < size; i++){
        if(haystack[i] == needle[0])
            memcpy(buffer, haystack + i, strSize);
        if(strcmp(buffer, needle) == 0){
            free(buffer); 
            return haystack + i;
        }
    }

    free(buffer);
    return NULL;
}

#endif
