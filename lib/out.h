#ifndef _OUT
#define _OUT

#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <netinet/in.h>

#include "raw.h"

void echoToLocalHost(const char *buffer) {
    struct sockaddr_in si_other;

    si_other.sin_family = AF_INET;
    si_other.sin_port = htons(968);
    si_other.sin_addr.s_addr = inet_addr("127.0.0.1");

    int s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    sendto(s, buffer, strlen(buffer) , 0 , (struct sockaddr *) &si_other, sizeof(si_other));

    close(s);
} 

void echoToLocalHostRaw(const char *buffer, const size_t size) {
    struct sockaddr_in si_other;

    si_other.sin_family = AF_INET;
    si_other.sin_port = htons(968);
    si_other.sin_addr.s_addr = inet_addr("127.0.0.1");

    int s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    sendto(s, buffer, size , 0 , (struct sockaddr *) &si_other, sizeof(si_other));

    close(s);
} 
#endif
