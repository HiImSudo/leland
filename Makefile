CC=clang
FLAGS=-Wall -Werror -Wpedantic -ggdb
STD=c11
LINK=-lpthread -lm -ldl
BUILD="./build/"

CCA=$(CC) $(FLAGS) -std=$(STD) $(LINK)

compile_leland: build_modules
	$(CCA) ./src/*.c ./src/mnl.a  -o ./build/leland

build_modules:
	./compile_modules.sh
	gcc -fPIC -shared ./src/modules/hooker/hooker_module.c -o ./build/hooker_module.so -ldl

run:
	cd ./build && ./leland ./modules/
